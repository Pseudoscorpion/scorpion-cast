#include <string>
#include <vector>
#include <iostream>
#include <cmath>

/*
GENERAL TODO:
*/

//checks a vector if a specific string is in it. useful for checking if a string is any of a specific type.
bool arrayMatch(std::string inputString, std::vector<std::string>&infoArray){
	for(int b = 0; b < infoArray.size(); ++b){
		if(inputString == infoArray[b]){
			return true;
		}
	}
	return false;
}

//class for tokens as an object
class token{
    /* type deciding how it will be handled
    number: a value
    operation: a "combiner" of two values
    metadata: parantheses, etc. second to be considered in handling order.
    variable: any variable
    instruction: first token read, will determine type of query
    */
    enum tokenType {
        number, operation, metadata,
        variable, instruction, invalid
    };
    //the actual data for the token. its type and what it contains.
    tokenType tokenType;
    std::string tokenData;
    public:
    //constructor for a new token, returning a token with a set type and value
    token tokenConstruct(std::string type, std::string data){
        token newToken;
        if(type == "number"){
            newToken.tokenType = number;
        }else
        if(type == "operation"){
            newToken.tokenType = operation;
        }else
        if(type == "metadata"){
            newToken.tokenType = metadata;
        }else
        if(type == "variable"){
            newToken.tokenType = variable;
        }else
        if(type == "instruction"){
            newToken.tokenType = instruction;
        }
        else{
            newToken.tokenType = invalid;
        }
        newToken.tokenData = data;
        return newToken;
    }
};
/*CLASS TODO:
Make it so that for each type there is a vector of strings that contain a bunch of times. Check if builtString matches each of these arrays.
*/
//class that handles all the magic
class tokenizer{
public:
  std::vector<token> tokenize(std::string inputSequence){
    /*Inputsequence is the input string. Tokensequence is the tokens it is converted into.
    Selectedcharacter is the current character being run through.
    Builtstring is a string being built. This can be a phrase or number. Main token is for using tokenconstruct.*/
    std::string inputSequence, builtString;
    std::vector<token> tokenSequence;
    char selectedCharacter;
    bool numberBuild, stringBuild;
    token main;
    //Vectors listing valid words to be pushed back.
    std::vector<std::string> instruction {"calculate", "convert"};
	
    //Iterates through the string and push backs tokens if it finds any
    for(int i = 0; i < inputSequence.length(); ++i){
      selectedCharacter = inputSequence[i];
      //If it is part of a number, it will push back each character of said number into builtstring.
      if(selectedCharacter == '.' || isdigit(selectedCharacter)){
        builtString.push_back(selectedCharacter);
        //Will set the flag that it is building a number to true
        bool numberBuild = true;
      }
      else{
        //If it is part of a word, it will push back each letter of said word into builtstring
        if(isalpha(selectedCharacter)){
          builtString.push_back(selectedCharacter);
          //And also set the flag that a word is being built to true.
          bool stringBuild = true;
        }

      //If there no longer is a new number or letter, or the character is anything else, it jumps here
      else{
        //This simply checks to see if a number has been built. If so, it will push back a new number token. Will also reset the built string.
        if(numberBuild){
          numberBuild = false;
          tokenSequence.push_back (main.tokenConstruct("number", builtString));
          builtString = "";
        }else
        //Checks if a word was built. Will push back a new token with the correct type by iterating through a vector.
        if(stringBuild){
          stringBuild = false;
          //Checks if the word is an instruction.
          if(builtString == "calculate"){
            tokenSequence.push_back(main.tokenConstruct("instruction", builtString));
          }
          builtString = "";
        }else
        //Handles non strings
        switch (selectedCharacter){
          case '+':
          tokenSequence.push_back(main.tokenConstruct("operation", "addition"));
          break;
          case '-':
          tokenSequence.push_back(main.tokenConstruct("operation", "subtraction"));
          break;
          case '/':
          tokenSequence.push_back(main.tokenConstruct("operation", "division"));
          break;
          case '*':
          tokenSequence.push_back(main.tokenConstruct("operation", "multiplication"));
          break;
          case '^':
          tokenSequence.push_back(main.tokenConstruct("operation", "exponent"));
          break;
          case '(':
          tokenSequence.push_back(main.tokenConstruct("metadata", "leftParentheses"));
          break;
          case ')':
          tokenSequence.push_back(main.tokenConstruct("metadata", "rightParentheses"));
          break;
          default:
          break;
          }
        }
      }
    }
	return tokenSequence;
  }
};

class tokenOperate{
	//Goes through a list of checks to even see if the token sequence is actually operatable
	bool tokenVectorValidate(std::vector<token>&sequence){
		int leftParentheses = 0;
		int rightParentheses = 0;
		
		for(int z = 0; z < sequence.size(); ++z){
			if(sequence[z].tokenData == "leftParentheses"){
				++leftParentheses;
			}
			if(sequence[z].tokenData == "rightParentheses"){
				++rightParentheses;
			}
		}
		if(leftParentheses != rightParentheses){
			return false;
		}
		return true;
	}
};

/*CLASS TODO: 
![DONE]! Create functions that take two tokens, and return one with a specific operation.
For the main function, first have it figure out if anything is misaligned, like unmatched brackets or whatever.
Second, add a function to rearrange the vector as needed.
Third, create ranges between brackets, then find 3 tokens, two with an operator, input both into one of the functions, return a new token, and delete the old ones.
Also, change it so "tokenize" returns a vector of tokens.
Also also, find locations in the main token vector of parentheses and create a for loop for each set of parantheses
that does the EMDAS of PEMDAS.
*/
class calculate{
  token main;
  
  //Takes two tokens, operates on them, returns token with result. Massive else if nest. 
  token operate(token a, token b, token operator){
	  if(operator.tokenData == "addition"){
		  return main.tokenConstruct("number", std::to_string((std::stod(a.tokenData)+std::stod(b.tokenData))));
	  }else 
		  if (operator.tokenData == "subtraction"){
			  return main.tokenConstruct("number", std::to_string((std::stod(a.tokenData)-std::stod(b.tokenData))));
		  }else
			  if (operator.tokenData == "division"){
				  return main.tokenConstruct("number", std::to_string((std::stod(a.tokenData)/std::stod(b.tokenData))));
			  }else
				  if (operator.tokenData == "multiplication"){
					  return main.tokenConstruct("number", std::to_string((std::stod(a.tokenData)*std::stod(b.tokenData))));
				  }else
					  if (operator.tokenData == "exponent"){
						  return main.tokenConstruct("number", std::to_string(std::pow(std::stod(a.tokenData),std::stod(b.tokenData))));
					  }
	else{
		return main.tokenConstruct("invalid", "");
	}
  }

};

//Main class.
/*CLASS TODO:
![DONE]! Add Input
Add Output
Create a bool that handles the program itself (on, off), then create a main loop function.
*/
class main{
	//Handles input.
	std::string input(){
		std::string userInput;
		
		std::cout << "Please enter desired string to be processed.\n";
		cin.ignore();
		std::getline(std::cin, userInput);
		return userInput;
	}
};

int main(){
	
  return 0;
}
